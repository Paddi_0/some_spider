from db import Base, engine
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy import Integer, String

class BqgBook(Base):
    """
    笔趣阁书
    """
    
    __tablename__ = 'bqg_book'
    __table_args__ = {'comment': '笔趣阁书籍表'}
    
    id: Mapped[str] = mapped_column(String, primary_key=True, comment='唯一标识')
    name: Mapped[str] = mapped_column(String, comment='书名')
    auther: Mapped[str] = mapped_column(String, comment='作者')
    category: Mapped[str] = mapped_column(String, comment='分类')
    link: Mapped[str] = mapped_column(String, comment = '链接')
    cover: Mapped[str] = mapped_column(String, comment='封面')
    
    def __repr__(self):
        return ("<BqgChapter(id='%s', name='%s', auther='%s', category='%s', link='%s', cover='%s')>" 
                % (self.id, self.name, self.auther, self.category, self.link, self.cover))

class BqgChapter(Base):
    """
    笔趣阁章节
    """
    
    __tablename__ = 'bqg_chapter'
    __table_args__ = {'comment': '笔趣阁书籍章节表'}
    
    id: Mapped[str] = mapped_column(String, primary_key=True, comment='唯一标识（book_id-chapter_number）')
    book_id: Mapped[int] = mapped_column(Integer, comment='书籍id')
    chapter_number: Mapped[int] = mapped_column(Integer, comment='章节编号')
    title: Mapped[str] = mapped_column(String, comment='章节标题')
    link: Mapped[str] = mapped_column(String, comment='章节链接')
    content: Mapped[str] = mapped_column(String, comment='章节正文')
    
    def __repr__(self):
        return ("<BqgChapter(id='%s', book_id='%d', chapter_number='%d', title='%s', link='%s', content='%s')>" 
                % (self.id, self.book_id, self.chapter_number, self.title, self.link, self.content))
        
Base.metadata.create_all(engine)