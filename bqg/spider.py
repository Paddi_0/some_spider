import requests, re, concurrent.futures, logging

from bs4 import BeautifulSoup
from string import Template
from ebooklib import epub
from entity import BqgBook, BqgChapter
from typing import List
from db import Session
from sqlalchemy import asc

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

"""可用站点
    https://www.bqg59.com
    https://www.bqg62.com
    https://www.bqg63.com
    https://www.bqg65.com
    https://www.bqg70.com
    https://www.bqg90.com
    https://www.bqgbe.com
    https://www.bq7.cc
    https://www.bqg600.cc
    https://www.bqglu.cc
"""
# 链接前缀
WEBSITE = 'https://www.bqg59.com'

headers = {
    'Host': 'www.bqg59.com',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:120.0) Gecko/20100101 Firefox/120.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2',
    'Connection': 'keep-alive',
    'Alt-Used': 'www.bqg59.com'
}


def request_html(url: str) -> str:
    """请求获取页面HTML

    Args:
        book_id (int): www.bqg59.com站点书籍编号

    Returns:
        str: HTML
    """
    
    try:
        response = requests.get(url, headers=headers)
        response.encoding = 'utf-8'
        return response.text
    except requests.exceptions.RequestException as e:
        logging.error(f"Error requesting {url}: {str(e)}")
        return None


  
def spider_bqg_book_info(book_id:int, html: str=None) -> BqgBook:
    """爬取书籍信息

    Args:
        book_id (int): www.bqg59.com站点书籍编号
        
        html (str): 书籍主页HTML

    Returns:
        BqgBook: 书籍对象
    """
    
    if html is None:
        html = request_html(WEBSITE + '/book/{}/'.format(book_id))
    
    book_catalogue_soup = BeautifulSoup(html, features="lxml")
    # 构建BqgBook对象
    book_name = book_catalogue_soup.find("meta", property="og:novel:book_name")["content"]
    book_author = book_catalogue_soup.find("meta", property="og:novel:author")["content"]
    book_category = book_catalogue_soup.find("meta", property="og:novel:category")["content"]
    book_link = book_catalogue_soup.find("meta", property="og:url")["content"]
    book_cover = book_catalogue_soup.find("meta", property="og:image")["content"]
    bqg_book = BqgBook(id=book_id, name=book_name, auther=book_author, category=book_category, link=book_link, cover=book_cover)
    
    return bqg_book



def spider_bqg_book_chapters(book_id:int, html: str=None) -> List[BqgChapter]:
    """爬取书籍章节

    Args:
        book_id (int): www.bqg59.com站点书籍编号
        
        html (str): 书籍主页HTML

    Returns:
        List[BqgChapter]: 章节列表
    """
    
    if html is None:
        html = request_html(WEBSITE + '/book/{}/'.format(book_id))
    
    book_catalogue_soup = BeautifulSoup(html, features="lxml")
    # 获取包含章节信息标签
    list_main = book_catalogue_soup.find('div', class_='listmain')
    dd_tag_list = list_main.find_all('dd')
    # 章节列表
    bqg_book_chapters = []
    for dd_tag in dd_tag_list:
        # 过滤期中混杂的一些无用标签
        if dd_tag.get('class') == ['more', 'pc_none']:
            continue
        # 只获取a标签
        a_tag = dd_tag.find('a')
        if not a_tag:
            continue
        # 解析有效数据
        title = a_tag.get_text()
        link = WEBSITE + a_tag.get('href')
        chapter_number = 0
        match = re.search(r'/(\d+)\.html$', link)
        if match:
            chapter_number = int(match.group(1))
        bqg_chapter = BqgChapter(id='-'.join([str(book_id), str(chapter_number)]), book_id=book_id, 
                                 chapter_number=chapter_number, title=title, link=link, content='')
        
        bqg_book_chapters.append(bqg_chapter)
        
    return bqg_book_chapters



def spider_chapter_content(chapter_link: str) -> str:
    """爬取章节内容

    Args:
        chapter_link (str): 章节链接

    Returns:
        str: 章节内容
    """
    
    content_html = request_html(chapter_link)
    content_soup = BeautifulSoup(content_html, features='lxml')
    chapter_content = content_soup.find('div', id='chaptercontent').text

    # 替换一下不需要的字符和内容
    chapter_content = (chapter_content
                       .replace('请收藏本站：https://www.bqg59.com。笔趣阁手机版：https://m.bqg59.com', '')
                       .replace('『点此报错』『加入书签』', ''))
    
    content_split = chapter_content.split('　　')
    p_template = Template('\t<p>$p</p>\r\n')
    p_result = []
    
    for c in content_split:
        if c.strip() == '': continue
        c = p_template.substitute(p=c)
        p_result.append(c)
    
    return ''.join(p_result)



def content_task(bqg_chapter: BqgChapter):
    """内容任务

    Args:
        bqg_chapter (BqgChapter): 章节对象
    """
    
    content = spider_chapter_content(bqg_chapter.link)
    bqg_chapter.content = content
    logging.info(f'【已爬取】{bqg_chapter.title}')



def get_book(book_id: int):
    """获取书籍

    Args:
        book_id (int): www.bqg59.com站点书籍编号

    Returns:
        (BqgBook, List[BqgChapter]): 书籍信息，章节列表
    """
    html = None
    
    session = Session()
    
    book_info = None
    book_info = session.get(BqgBook, book_id)
    
    if book_info is None:
        html = request_html(WEBSITE + '/book/{}/'.format(book_id))
        book_info = spider_bqg_book_info(book_id,html)
        session.add(book_info)
        session.commit()
        logging.info(f'【{book_info.name}-{book_info.name}】书籍信息入库获取成功')
    else:
        logging.info(f'【{book_info.name}-{book_info.auther}】书籍信息在本地库中已存在')
        
    chapter_total = session.query(BqgChapter).filter(BqgChapter.book_id==book_id).count()
    book_chapters = []
    if chapter_total == 0:
        book_chapters = spider_bqg_book_chapters(book_id,html)
        # 多线程获取每个章节的正文内容入库
        with concurrent.futures.ThreadPoolExecutor(max_workers=50) as executor:
            futures = {executor.submit(content_task, chapter): chapter for chapter in book_chapters}
            # 等待所有任务完成
            concurrent.futures.wait(futures)
            
        logging.info(f'已爬取所有章节，共【{len(book_chapters)}】个章节')
        chunk_size = 100
        for i in range(0, len(book_chapters), chunk_size):
            chunk = book_chapters[i:i+chunk_size]
            session.add_all(chunk)
        session.commit()
        logging.info(f'【{book_info.name}-{book_info.auther}】书籍章节信息入库成功，共{len(book_chapters)}章')
    else:
        book_chapters = session.query(BqgChapter).filter(BqgChapter.book_id==book_id).order_by(asc(BqgChapter.chapter_number)).all()
        logging.info(f'【{book_info.name}-{book_info.auther}】书籍章节信息在本地库中已存在')
    session.close()    
    return book_info, book_chapters



def build_chapter(title: str, content: str) -> str:
    """构建章节

    Args:
        title (str): 标题
        
        content (str): 内容

    Returns:
        str: 带标签的章节
    """
    template = Template('<h3>$title</h3>\r\n$content')
    return template.substitute(title=title, content=content)



def out_epub(book_info: BqgBook, book_chapters: List[BqgChapter], out_path=None):
    logging.info(f'准备输出【{book_info.name}】epub格式文件')
    book = epub.EpubBook()
    
    # 设置元数据
    book.set_title(book_info.name)
    book.set_identifier(f'{book_info.name}-{book_info.auther}')
    book.add_author(book_info.auther)
    book.set_language('zh-CN')
    
    # 设置封面
    book.set_cover('cover.jpg', requests.get(book_info.cover).content)
    
    # 设置样式
    with open('static/main.css', 'r') as file:
            main_css = file.read()
    css_file = epub.EpubItem(uid='style_main', file_name='main.css', media_type='text/css', content=main_css)
    book.add_item(css_file)
        
    for idx, book_chapter in enumerate(book_chapters):
        chapter = epub.EpubHtml(title=book_chapter.title, file_name=f'chapter_{idx + 1}.html', 
                                lang='zh-CN', content=build_chapter(book_chapter.title, book_chapter.content))
        chapter.add_link(href='main.css', rel='stylesheet', type='text/css')
        book.add_item(chapter)
        # 创建章节链接并设置目录结构
        chapter_link = epub.Link(f'chapter_{idx + 1}.html', book_chapter.title, f'chapter{idx + 1}')
        book.toc.append(chapter_link)
        
    # 设置封面和章节顺序
    book.spine = ['cover'] + [f'chapter_{idx}' for idx in range(1, len(book_chapters)+1)]
    
    book.add_item(epub.EpubNcx())
    book.add_item(epub.EpubNav())
    
    # 构建 EPUB 文件
    epub.write_epub(f'{book_info.name}.epub', book, {})
    logging.info(f'已输出【{book_info.name}】epub格式文件')
    
    
# end def

if __name__ == '__main__':
    """
        502 斗破苍穹
        673 凡人修仙传
    """
    # book_info, book_chapters = get_book(673)
    # out_epub(book_info, book_chapters)
    response = requests.get('https://www.bqg59.com/user/search.html?q=%E4%B8%89%E4%BD%93', headers=headers).text
    # response.encoding = 'utf-8'
    print(response)
